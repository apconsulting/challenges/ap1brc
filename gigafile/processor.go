package gigafile

import "io"

type Processor interface {
	Process(filename string, out io.Writer)
	Reset()
}
