2024 - 1 Billion Rows Challenge (GO)
=============================

The challenge
----------------------------

La sfida consiste nell'elaborare il file measurements.txt,
ottenuto unzippando [measurements.zip](https://drive.google.com/file/d/178n7QcDyd84Qixhnp3J84wAR8OWDkmG5/view?usp=sharing), nel minor tempo possibile. Lo zip è scaricabile a questo indirizzo:

https://drive.google.com/file/d/178n7QcDyd84Qixhnp3J84wAR8OWDkmG5/view?usp=sharing

Il file è composto da 1 miliardo di righe, ciascuna della forma
```
<nome città>;<temperatura rilevata>\n
```

Esempio:
```
Venezia;23.7
New York;-7.0
```
La codifica del file è UTF-8, i nomi delle città sono lunghi al
massimo 100 byte (!= caratteri).
Le temeprature sono sempre espresse come numero con una sola cifra
decimale, nel range [-99.9, 99.9].

L'output dell'elaborazione deve essere un testo della forma:

{<nome città>=<min>/<mean>/max>, ...}

dove le temeprature sono espresse nello stesso formato del file di input.
Tornando all'esempio, l'output corrispondente sarebbe:

{Venezia=23.7/23.7/23.7, New York=-7.0/-7.0/-7.0}

Modalità di partecipazione
--------------------------------

In questa repository è incluso il codice
di un programma funzionante che implementa, nel package
naive, una corretta quanto inefficiente elaborazione del file. 
Questo package può essere usato come punto di partenza!

La soluzione deve rispettare i seguenti vincoli:
 - Deve essere implementata in GO! (https://go.dev/)
 - il tipo di dato che elabora il file deve implementare
   l'interfaccia gigafile.Processor 
 - Il package deve superare la suite di test definita nel file
   main_test.go

Nota bene:
 - La versione naive della soluzione può impiegare dai 5 ai
   10 minuti per terminare l'elaborazione e necessita di almeno
   16 GiB di RAM
 - Per il calcolo della media discostarsi dalla procedura
   utilizzata nel package naive solo se assolutamente
   sicuri di quello che si sta facendo
 - Si può sfruttare la PGO (Profile Guided Optimization),
   procedendo nel seguente modo:
    1. Lanciare il proprio eseguibile come segue:
       .\ap1brc.exe -cpuprofile default.pgo
    2. Ricompilare il codice:
       go build -pgo .\default.pgo
  - La cartella beatme contiene l'eseguibile della versione "campione in carica".
  - Una buona soluzione ha un tempo di esecuzione sotto al minuto.
  - Il file banchmark_io_test.go contiene il benchmarking di alcune strategie per la lettura.

Per partecipare è sufficiente inviare la cartella zippata del proprio package contenente la soluzione all'indirizzo email: areasviluppo@apconsulting.net

__Includere nella soluzione ESCLUSIVAMENTE il sorgente del package implementato.__