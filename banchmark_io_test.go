package main

import (
	"io"
	"log"
	"os"
	"testing"

	"github.com/ncw/directio"
	"golang.org/x/exp/mmap"
)

const BUFSIZE = directio.BlockSize * (1 << 8)

func ioReadAt(newBufEachRead bool) {
	in, e := os.Open("measurements.txt")
	if e != nil {
		log.Fatalln(e)
	}
	defer in.Close()

	var off int64

	var buf []byte
	if !newBufEachRead {
		buf = make([]byte, BUFSIZE)
	}
	for {
		if newBufEachRead {
			buf = make([]byte, BUFSIZE)
		}
		_, e := in.ReadAt(buf, off)
		if e == io.EOF {
			break
		}
		if e != nil {
			log.Fatalln(e)
		}
		off += BUFSIZE
	}
}

func ioReadAtPrealloc() {
	const BUFSIZE = 928 * 1024
	in, e := os.Open("measurements.txt")
	if e != nil {
		log.Fatalln(e)
	}
	defer in.Close()
	info, e := in.Stat()
	if e != nil {
		log.Fatalln(e)
	}
	size := info.Size()

	var off int64

	var buf []byte
	memory := make([]byte, (size/BUFSIZE+1)*BUFSIZE)
	for {
		if off >= size {
			break
		}

		buf = memory[off : off+BUFSIZE]

		_, e := in.ReadAt(buf, off)
		if e == io.EOF {
			break
		}
		if e != nil {
			log.Fatalln(e)
		}
		off += BUFSIZE
	}
}

func ioRead(newBufEachRead bool) {
	in, e := os.Open("measurements.txt")
	if e != nil {
		log.Fatalln(e)
	}
	defer in.Close()

	var buf []byte
	if !newBufEachRead {
		buf = make([]byte, BUFSIZE)
	}
	for {
		if newBufEachRead {
			buf = make([]byte, BUFSIZE)
		}
		_, e := in.Read(buf)
		if e == io.EOF {
			break
		}
		if e != nil {
			log.Fatalln(e)
		}
	}
}

func directIoRead(newBufEachRead bool) {
	// const BUFSIZE = 928 * 1024

	in, e := directio.OpenFile("measurements.txt", os.O_RDONLY, 0666)
	if e != nil {
		log.Fatalln(e)
	}
	defer in.Close()

	var buf []byte
	if !newBufEachRead {
		buf = directio.AlignedBlock(BUFSIZE)
	}

	for {
		if newBufEachRead {
			buf = directio.AlignedBlock(BUFSIZE)
		}
		_, e := in.Read(buf)
		if e == io.EOF {
			break
		}
		if e != nil {
			log.Fatalln(e)
		}
	}
}

func mmapIoReadAt(newBufEachRead bool) {
	in, e := mmap.Open("measurements.txt")
	if e != nil {
		log.Fatalln(e)
	}
	defer in.Close()

	var off int64

	var buf []byte
	if !newBufEachRead {
		buf = make([]byte, BUFSIZE)
	}
	for {
		if newBufEachRead {
			buf = make([]byte, BUFSIZE)
		}
		_, e := in.ReadAt(buf, off)
		if e == io.EOF {
			break
		}
		if e != nil {
			log.Fatalln(e)
		}
		off += BUFSIZE
	}
}

func BenchmarkIoReadAt(b *testing.B) {
	for n := 0; n < b.N; n++ {
		ioReadAt(false)
	}
}

func BenchmarkIoReadAtPrealloc(b *testing.B) {
	for n := 0; n < b.N; n++ {
		ioReadAtPrealloc()
	}
}

func BenchmarkIoReadAtAlloc(b *testing.B) {
	for n := 0; n < b.N; n++ {
		ioReadAt(true)
	}
}

func BenchmarkIoRead(b *testing.B) {
	for n := 0; n < b.N; n++ {
		ioRead(false)
	}
}

func BenchmarkDirectIoRead(b *testing.B) {
	for n := 0; n < b.N; n++ {
		directIoRead(false)
	}
}

func BenchmarkMmapIoReadAt(b *testing.B) {
	for n := 0; n < b.N; n++ {
		mmapIoReadAt(false)
	}
}
