package naive

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"slices"
	"strconv"
	"strings"

	"golang.org/x/exp/maps"
)

type stats struct {
	n, sum, min, max float64
	name             string
}

func (s stats) mean() string {
	m := float64(s.sum) / float64(s.n)
	return strconv.FormatFloat(m, 'f', 1, 64)
}

func (s stats) print(out io.Writer) {
	str := fmt.Sprintf("%s=%.1f/%s/%.1f", s.name, s.min, s.mean(), s.max)
	out.Write([]byte(str))
}

type Results map[string]stats

func New() Results {
	return make(Results, 10000)
}

func (m Results) update(key string, value float64) {
	s, ok := m[key]
	if ok {
		s.n++
		s.sum += value
		if s.min > value {
			s.min = value
		}
		if s.max < value {
			s.max = value
		}
		m[key] = s
		return
	}
	m[key] = stats{1, value, value, value, key}
}

func (m Results) print(out io.Writer) {
	out.Write([]byte{'{'})
	keys := maps.Keys(m)
	slices.SortFunc(keys, strings.Compare)
	for i, k := range keys {
		if i > 0 {
			out.Write([]byte{',', ' '})
		}
		m[k].print(out)
	}
	out.Write([]byte{'}', '\n'})
}

func (m Results) Process(filename string, out io.Writer) {
	r, e := os.Open(filename)
	if e != nil {
		log.Fatalln(e)
	}
	defer r.Close()

	buffer, err := os.ReadFile(filename)
	if err != nil {
		log.Fatalln(err)
	}

	m.processBuffer(buffer)

	m.print(out)
}

func (m Results) processBuffer(buf []byte) {
	const EOL = 0x0A

	pos := 0
	for {
		i := bytes.IndexByte(buf[pos:], ';')
		if i < 0 {
			return
		}
		key := string(buf[pos : pos+i])

		pos += i + 1

		i = bytes.IndexByte(buf[pos:], EOL)
		value, err := strconv.ParseFloat(string(buf[pos:pos+i]), 64)
		if err != nil {
			log.Fatalln(err)
		}

		pos += i + 1
		m.update(key, value)
	}
}

func (m Results) Reset() {
	clear(m)
}
