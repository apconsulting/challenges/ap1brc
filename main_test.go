package main

import (
	"bytes"
	"os"
	"testing"
)

func compareOutput(output []byte, refFilename string) (bool, error) {
	refContent, err := os.ReadFile(refFilename)
	if err != nil {
		return false, err
	}
	return bytes.Equal(output, refContent), nil
}

func TestProcessRounding(t *testing.T) {
	processor.Reset()
	out := bytes.NewBuffer(make([]byte, 0, 1<<10))
	processor.Process("data/measurements-rounding.txt", out)
	ok, err := compareOutput(out.Bytes(), "data/measurements-rounding.out")
	if err != nil {
		t.Fatal("error comparing output", err)
	}
	if !ok {
		t.Fail()
	}
}

func TestProcess10000Keys(t *testing.T) {
	processor.Reset()
	out := bytes.NewBuffer(make([]byte, 0, 1<<10))
	processor.Process("data/measurements-10000-unique-keys.txt", out)
	ok, err := compareOutput(out.Bytes(), "data/measurements-10000-unique-keys.out")
	if err != nil {
		t.Fatal(err)
	}
	if !ok {
		t.Fail()
	}
}

// In settings.json set testTimeout to at least 300s
func TestProcess(t *testing.T) {
	processor.Reset()
	out := bytes.NewBuffer(make([]byte, 0, 1<<10))
	processor.Process("measurements.txt", out)
	ok, err := compareOutput(out.Bytes(), "data/measurements.out")
	if err != nil {
		t.Fatal(err)
	}
	if !ok {
		t.Fail()
	}
}
